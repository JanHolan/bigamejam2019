﻿using System.Linq.Expressions;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
[CustomEditor(typeof(StateBase))]
public class StateBaseEditor : Editor
{
    SerializedProperty stateName;
    SerializedProperty onStateExit;
    SerializedProperty linksProp;

    private bool linksExpand = true;
    private int linksSize = 0;
    private StateLink[] links = new StateLink[0];
    void OnEnable()
    {
        // Setup the SerializedProperties.
        stateName = serializedObject.FindProperty ("stateName");
        onStateExit = serializedObject.FindProperty ("onStateExit");
        linksProp = serializedObject.FindProperty("links");
    }
    public override void OnInspectorGUI()
    {
        // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
        serializedObject.Update ();
        
        DrawDefaultInspector();   
        EditorGUILayout.Separator();
        
        linksExpand = EditorGUILayout.Foldout(linksExpand, "Links targets");
        if (linksExpand)
        {
            for (int i = 0; i < linksProp.arraySize; i++)
            {
                var nextState = linksProp.GetArrayElementAtIndex(i).FindPropertyRelative("toState");
                string linkString = "Element " + i + " -> NOT LINKED";
                if (nextState != null && nextState.objectReferenceValue != null)
                    linkString = "Element " + i + " -> " + (nextState.objectReferenceValue as StateBase).stateName;
                EditorGUILayout.LabelField(linkString);
            }
            
        }
        

        // Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.

    }
}

