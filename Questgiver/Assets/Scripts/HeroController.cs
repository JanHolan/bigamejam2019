﻿using System;
using System.Collections.Generic;
using PathCreation;
using Silicomrades;
using UnityEngine;
using Random = System.Random;

public enum HeroState
{
    WaitingForQuest,
    Questing,
    ComingBack,
    WaitingForReward
};

public class HeroController : MonoBehaviour
{
    private Queue<PathController> currentPath;
    private float currentTraveledDistance;

    private WorldLocationController currentGoal;


    public StateLink currentQuest;
    public WorldLocationController questLocation;
    public WorldLocationController questStartLocation;
    
    private WorldLocationController currentLocation;
    [SerializeField] private float speed = 5f;
    private bool following = false;

    public string heroName;
    public HeroState heroState;
    public Sprite heroImage;
    public Sprite heroThumbImage;
    public Sprite[] imagePool;
    public Sprite[] thumbImagePool;
    public int minWaiting = 10;
    public int maxWaiting = 30;
    public Timer timer;

    public int minBlabber = 2;
    public int maxBlabber = 3;
    public Timer blabberTimer;

    // Start is called before the first frame update
    void Awake()
    {
        heroName = HeroNameGenerator.inst.GetName();
        heroState = HeroState.WaitingForQuest;
        if (imagePool.Length > 0)
        {
            var index = UnityEngine.Random.Range(0, imagePool.Length);
            heroImage = imagePool[index];
            heroThumbImage = thumbImagePool[index];
        }
        timer = new Timer(UnityEngine.Random.Range(minWaiting, maxWaiting));
        blabberTimer = new Timer(UnityEngine.Random.Range(minBlabber, maxBlabber));
    }

    // Update is called once per frame
    private void Update()
    {
        blabberTimer.DoUpdate();
        if (blabberTimer.IsDone())
        {
            var blabber = HeroNameGenerator.inst.GetBlabber();            
            var color = HeroNameGenerator.inst.GetColor();
            ChatManager.inst.PlayerChat(heroName, blabber, color);
            blabberTimer = new Timer(UnityEngine.Random.Range(minBlabber, maxBlabber));
        }
        
        if (heroState == HeroState.WaitingForReward || heroState == HeroState.WaitingForQuest)
            timer.DoUpdate();
        if(following)
            FollowPath();
    }


    public void StartQuest(WorldLocationController location, StateLink quest)
    {
        this.enabled = true;
        currentLocation = GetClosestWorldLocation();
        heroState = HeroState.Questing;
        currentQuest = quest;
        questLocation = location;
        questStartLocation = currentLocation;
        WalkTo(location);
    }

    private void WalkTo(WorldLocationController worldLocation)
    {

        currentGoal = worldLocation;
        currentPath = new Queue<PathController>();
        PathController[] path = currentLocation.FindPathTo(worldLocation);
        for (int i = 0; i < path.Length; i++)
        {
            currentPath.Enqueue(path[i]);
        }
        currentTraveledDistance = currentPath.Peek().path.path.GetClosestDistanceAlongPath(transform.position);
        following = true;
    }

    private void FollowPath()
    {
        currentTraveledDistance += speed * Time.deltaTime;
        transform.position = currentPath.Peek().path.path.GetPointAtDistance(currentTraveledDistance, EndOfPathInstruction.Reverse);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!following) return;

        if (!other.CompareTag("WorldLocation"))
            return;

        currentLocation = other.GetComponent<WorldLocationController>();
        if (currentPath.Count > 0 && currentLocation == currentPath.Peek().to)
        {
            currentPath.Dequeue();
            if (currentPath.Count <= 0)
            ReachedGoal();
            else
            {   
                currentTraveledDistance = currentPath.Peek().path.path.GetClosestDistanceAlongPath(transform.position);
            }
        }
    }

    private void ReachedGoal()
    {
        following = false;
        switch (heroState)
        {
            case HeroState.Questing:
            {
                var questTarget = currentQuest.fromState.GetComponent<LocationStateManager>();
                if (!questLocation.HasLocation(questTarget) || !questTarget.TryTransition(currentQuest, QuestComplete))
                    QuestFailed();
                break;
            }
            case HeroState.ComingBack:
                heroState = HeroState.WaitingForReward;
                timer = new Timer(UnityEngine.Random.Range(minWaiting, maxWaiting));
                questStartLocation.GetMainLocation().AddReturningHero(this);
                TutorialManager.inst.tutorialEvent(TutorialTriggers.HERO_BACK);
                break;
        }
    }

    private void QuestFailed()
    {
        ChatManager.inst.PlayerChat(heroName, "This is $$$, this quest is bugged!");

        if (TutorialManager.inst.finished)
        {
            GameManager.inst.LifeLost("Quest cannot be solved");
        }
        else
        {
            TutorialManager.inst.FailedQuest();
        }
        Destroy(gameObject);
    }

    private void QuestComplete()
    {
        GetComponent<AudioSource>().Play();
        GameManager.inst.QuestCompleted(heroName);
        currentQuest = null;
        heroState = HeroState.ComingBack;
        WalkTo(questStartLocation);
    }

    /// <summary>
    /// /////DEBUG
    /// </summary>

    [Space] [Header("-----------DEBUG--------")]
    public StateBase DEBUGQuestToStart;

    public LocationStateManager DEBUGlocationOfQuest;
    
    [ContextMenu("Start Debug Quest")]
   public void StartQuestDebug()
   {
       currentLocation = GetClosestWorldLocation();
       Debug.Log(currentLocation.name);
       //StartQuest(DEBUGlocationOfQuest,DEBUGQuestToStart.GetQuests()[0]);
   }
   
   WorldLocationController GetClosestWorldLocation()
   {
       WorldLocationController[] locations = FindObjectsOfType<WorldLocationController>();
       WorldLocationController tMin = null;
       float minDist = Mathf.Infinity;
       Vector3 currentPos = transform.position;
       foreach (WorldLocationController t in locations)
       {
           float dist = Vector3.Distance(t.transform.position, currentPos);
           if (dist < minDist)
           {
               tMin = t;
               minDist = dist;
           }
       }
       return tMin;
   }
}
