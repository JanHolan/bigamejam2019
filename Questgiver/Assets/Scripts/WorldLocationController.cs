﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WorldLocationController : MonoBehaviour
{
    [SerializeField]
    private PathController[] connectedPaths;


    public void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
        {
            GameManager.inst.MoveCGToLocation(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        /*
        foreach (var path in connectedPaths)
        {
            path.from = this;
        }
        */
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    

    public PathController[] FindPathTo(WorldLocationController worldLocation)
    {
        if(worldLocation == this)
            return new PathController[0];
        
        Queue<WorldLocationController> openWorldLoc = new Queue<WorldLocationController>(1);
        Dictionary<WorldLocationController, WorldLocationController> predecesor = new Dictionary<WorldLocationController, WorldLocationController>();
        List<WorldLocationController> visited = new List<WorldLocationController>();
        visited.Add(this);
        predecesor[this] = null;
        openWorldLoc.Enqueue(this);
        while (openWorldLoc.Count > 0)
        {
            var nextLoc = openWorldLoc.Dequeue();

            
            if (nextLoc == worldLocation)
                return ReconstructPath(nextLoc, predecesor);

            foreach (var nieg  in nextLoc.GetNieghbourgs())
            {
                if (visited.Contains(nieg))
                    continue;
                
                predecesor[nieg] = nextLoc;
                openWorldLoc.Enqueue(nieg);
                visited.Add(nieg);
            }
        }

        return new PathController[0];
    }

    private PathController[] ReconstructPath(WorldLocationController from, Dictionary<WorldLocationController, WorldLocationController> predecesor)
    {
        List<PathController> pathReverse = new List<PathController>();
        WorldLocationController prev = predecesor[from];
        WorldLocationController prevprev = from;
        do{
        pathReverse.Add(prev.GetPathToNieghbourg(prevprev));
        prev = predecesor[prev];
        prevprev = predecesor[prevprev];
        }
        while (prev != null) ;
        
        pathReverse.Reverse();
        return pathReverse.ToArray();
    }

    private WorldLocationController[] GetNieghbourgs()
    {
        WorldLocationController[] niegs = new WorldLocationController[connectedPaths.Length];
        for (int i = 0; i < connectedPaths.Length; i++)
        {
            niegs[i] = connectedPaths[i].to;
        }

        return niegs;
    }

    public PathController GetPathToNieghbourg(WorldLocationController nieg)
    {
        foreach (var path in connectedPaths)
        {
            if (path.to == nieg)
                return path;
        }

        return null;
    }

    public LocationStateManager GetMainLocation()
    {
        foreach(var loc in GetComponentsInChildren<LocationStateManager>())
            if (!loc.character)
                return loc;
        
        return null;
    }

    public bool HasLocation(LocationStateManager questTarget)
    {
        foreach(var loc in GetComponentsInChildren<LocationStateManager>())
            if (loc == questTarget)
                return true;

        return false;
    }
}
