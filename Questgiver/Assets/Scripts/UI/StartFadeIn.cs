﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class StartFadeIn : MonoBehaviour {

    [SerializeField] float fadeInDuration = 1f;
    private Image _image;

    // Use this for initialization
	void Start ()
	{
		_image = GetComponent<Image>();
		_image.DOFade(0, fadeInDuration).SetEase(Ease.InQuint).SetUpdate(true) ;
	}
	

    public void FadeOut(float time, Action onComplete)
    {
        _image.DOFade(1, fadeInDuration).SetEase(Ease.InQuint).SetUpdate(true).OnComplete(() => onComplete());
    }
}
