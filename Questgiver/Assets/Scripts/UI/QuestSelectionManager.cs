﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class QuestSelectionManager : MonoBehaviour
{
    private QuestOnLocationManager qlm;
    private SentToQuestButtonController btc;
    private void Start()
    {
        qlm = FindObjectOfType<QuestOnLocationManager>();
        btc = FindObjectOfType<SentToQuestButtonController>();
    }

    public GameObject toHide;
    private bool shown = true;
    public void  Deselect()
    {
        
        qlm.Clear();
        
        btc.Deselect();
        
        foreach (var bt in GetComponentsInChildren<Button>())
        {
            bt.GetComponent<Image>().color = Color.white;
        }
    }

    public void Hide()
    {
        foreach(var go in GetComponentsInChildren<Button>())
            go.gameObject.SetActive(false);
        
        foreach(var go in toHide.GetComponentsInChildren<Button>(true))
            go.gameObject.SetActive(false);
        
        
        qlm.Clear();
        shown = false;
    }

    public void Show()
    {
        if (shown)
            return;

        shown = true;
        foreach(var go in GetComponentsInChildren<Button>(true))
            go.gameObject.SetActive(true);
        
        foreach(var go in toHide.GetComponentsInChildren<Button>(true))
            go.gameObject.SetActive(true);
        
        qlm.Clear();
    }
}
