﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectQuestButton : MonoBehaviour
{
    // Start is called before the first frame update
    public WorldLocationController target;
    public StateLink quest;

    public Image selectedImage;
    public Image activeImage;
    public bool selected = false;
    private SentToQuestButtonController stq;
    void Awake()
    {
        GetComponent<Button>().onClick.AddListener(SelectQuest);
        stq = FindObjectOfType<SentToQuestButtonController>();
    }

    public void SelectQuest()
    {
        stq.SelectQuest(target,quest);
        foreach (var bt in transform.parent.GetComponentsInChildren<SelectQuestButton>())
        {
            bt.Deselect();
        }

        selected = true;

    }

    public void Deselect()
    {
        selected = false;
        
        selectedImage.gameObject.SetActive(false);

    }
    
    // Update is called once per frame
    void Update()
    {
        if (!selected)
        {
            Deselect();
            if (quest.IsActive(target))
                activeImage.gameObject.SetActive(true);
            else
                activeImage.gameObject.SetActive(false);
        }
        else
        {
            Select();

        }
    }

    public void Select()
    {
        selectedImage.gameObject.SetActive(true);
    }

}
