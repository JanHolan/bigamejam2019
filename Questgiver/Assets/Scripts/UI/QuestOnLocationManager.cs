﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuestOnLocationManager : MonoBehaviour
{
    [SerializeField] private GameObject buttonPrefab;

    [SerializeField] private LocationStateManager dragon;
    [SerializeField] private LocationStateManager princess;

    [SerializeField] private Image locImage;
    [SerializeField] private Image prImage;
    [SerializeField] private Image drImage;

    
    public WorldLocationController lastLocation;
    // Start is called before the first frame update
    void Start()
    {
       
        princess = GameObject.Find("Princess").GetComponent<LocationStateManager>();
        dragon = GameObject.Find("Dragon").GetComponent<LocationStateManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void FillWith(WorldLocationController location)
    {
        Clear();
        
        lastLocation = location;
        var quests = location.GetMainLocation().GetAllQuests();
        CreateButtonsFor(quests);
        locImage.color = Color.yellow;
    }

    public void FillWithCharacters()
    {
        Clear();
        CreateButtonsFor(GameManager.inst.GetAllCharacterQuests());
    }

    public void FillWithDragon()
    {
        Clear();
        CreateButtonsFor(dragon.GetAllQuests());
        drImage.color = Color.yellow;
    }
    
    public void FillWithPrincess()
    {
        Clear();
        CreateButtonsFor(princess.GetAllQuests());
        prImage.color = Color.yellow;
    }

    public void FillWithLastLocation()
    {
        FillWith(lastLocation);
        locImage.color = Color.yellow;
    }

    private void CreateButtonsFor(StateLink[] quests)
    {
        foreach (var quest in quests)
        {
            var newButt = GameObject.Instantiate(buttonPrefab, transform);

            if (quest.linkName == "")
            {
                quest.linkName = quest.fromState.stateName + " -> " + quest.toState.stateName;
            }

            newButt.GetComponentInChildren<TextMeshProUGUI>().text = quest.linkName;
            var btScript = newButt.GetComponent<SelectQuestButton>();
            btScript.quest = quest;
            btScript.target = lastLocation;
        }
    }

    public void Clear()
    {
        foreach (Transform child in transform)
            GameObject.Destroy(child.gameObject);

        prImage.color = Color.white;
        drImage.color = Color.white;
        locImage.color = Color.white;
        
    }
}
