﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.UI.Image;

public class SentToQuestButtonController : MonoBehaviour
{

    private StateLink selectedQuest;

    private WorldLocationController selectedLocation;

    private Button bt;
    
    private TextMeshProUGUI questText;
    [SerializeField]
    private GameObject questPanel;
    [SerializeField]
    private Image bgSlot;
    [SerializeField]
    private Image bgSlotR;
    [SerializeField]
    private Sprite questionmark;
    [SerializeField]
    private Sprite exclammark;

    private QuestSelectionManager qsm;

    [SerializeField] private GameObject questRewardContainer;
    // Start is called before the first frame update
    void Start()
    {
        bt=GetComponent<Button>();
            bt.onClick.AddListener(SendHero);
            questText = GameObject.Find("QuestText").GetComponent<TextMeshProUGUI>();

            Hide();
            qsm = FindObjectOfType<QuestSelectionManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.inst.selectedHero == null)
        {
            Hide();
            qsm.Hide();
        }
        
        if (GameManager.inst.selectedHero != null &&
            GameManager.inst.selectedHero.heroState == HeroState.WaitingForQuest)
        {
            HideR();
            qsm.Show();
        }

        if (GameManager.inst.selectedHero != null &&
            GameManager.inst.selectedHero.heroState == HeroState.WaitingForReward)
        {
           ShowRewardPlayer();
        }
        
    }
    [ContextMenu("SendHero")]
    public void SendHero()
    {
        if(GameManager.inst.selectedHero.heroState == HeroState.WaitingForReward)
            GameManager.inst.GiveRewardToHero();
        else
        {
            GameManager.inst.selectedHero.StartQuest(selectedLocation,selectedQuest);
            GameManager.inst.QGwloc.GetMainLocation().heroList.Remove(GameManager.inst.selectedHero);
            qsm.Deselect();
        }


    }

    public void SelectQuest(WorldLocationController location,  StateLink quest)
    {
        selectedQuest = quest;
        selectedLocation = location;
        ShowQuestInfo(quest);

    }

    public void Deselect()
    {
        selectedQuest = null;
        selectedLocation = null;
        Hide();
    }

    private bool showingQ = true;
    public void ShowQuestInfo(StateLink quest)
    {
        HideR();
        questText.text = quest.linkActionDescription == "" ? "Missing quest description" : quest.linkActionDescription;
        if (showingQ)
            return;
        showingQ = true;
        questPanel.SetActive(true);
        foreach (var img in questPanel.GetComponentsInChildren<Image>())
            if(img != bgSlot)
            img.DOFade(1, 0.2f);

        bgSlot.DOFade(0.5f, 0.2f);
        bgSlot.sprite = exclammark;
        bt.GetComponentInChildren<TextMeshProUGUI>().text = "Send to Quest!";
        
        foreach (var txt in questPanel.GetComponentsInChildren<TextMeshProUGUI>())
            txt.DOFade(1, 0.2f);

        bt.interactable = true;
        qsm.Show();
    }

    public void HideQ()
    {
        if (!showingQ)
            return;
        showingQ = false;
        foreach (var img in questPanel.GetComponentsInChildren<Image>())
            
            img.DOFade(0, 0.2f);
        
        foreach (var txt in questPanel.GetComponentsInChildren<TextMeshProUGUI>())
            txt.DOFade(0, 0.2f);

        bt.interactable = false;
    }
    
    public void HideR()
    {
        if (!showingR)
            return;
        showingR = false;
        foreach (var img in questRewardContainer.GetComponentsInChildren<Image>())
            
            img.DOFade(0, 0.2f).OnComplete(() => questRewardContainer.SetActive(false));;
        
        foreach (var txt in questRewardContainer.GetComponentsInChildren<TextMeshProUGUI>())
            txt.DOFade(0, 0.2f);

        questRewardContainer.GetComponentInChildren<Button>().interactable = false;
    }

    private bool showingR = true;
    public void ShowRewardPlayer()
    {
        if (showingR)
            return;
        showingR = true;
        questRewardContainer.SetActive(true);
        foreach (var img in questRewardContainer.GetComponentsInChildren<Image>())
            if(img != bgSlotR)
                img.DOFade(1, 0.2f);
        bgSlotR.DOFade(0.5f, 0.2f);
        
        foreach (var txt in questRewardContainer.GetComponentsInChildren<TextMeshProUGUI>())
            txt.DOFade(1, 0.2f);

        questRewardContainer.GetComponentInChildren<Button>().interactable = true;
        HideQ();
        qsm.Hide();
    }

    public void Hide()
    {
        HideQ();
        HideR();
    }

    public void GiveRewardClick()
    {
        GameManager.inst.GiveRewardToHero();
    }

    public void UpdateNow()
    {
        Update();
    }
}
