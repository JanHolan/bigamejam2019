﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LocationButtonController : MonoBehaviour
{

    [SerializeField] private WorldLocationController assignedLocation;

    private Button bt;

    private QuestOnLocationManager qlm;
    // Start is called before the first frame update
    void Start()
    {
        GetComponentInChildren<TextMeshProUGUI>().text = assignedLocation.GetMainLocation().name;
        bt = GetComponent<Button>();
        bt.onClick.AddListener(OnClick);
        qlm = GameObject.FindObjectOfType<QuestOnLocationManager>();

    }

    // Update is called once per frame
    void Update()
    {
            bt.interactable = GameManager.inst.QGwloc != assignedLocation;
            if(qlm.lastLocation == assignedLocation)
                GetComponent<Image>().color = Color.yellow;
            else
            {
                GetComponent<Image>().color = Color.white;
            }
    }

    public void OnClick()
    {
        qlm.FillWith(assignedLocation);
       /* foreach (var bt in transform.parent.GetComponentsInChildren<Button>())
        {
            bt.GetComponent<Image>().color = Color.white;
        }
        
        GetComponent<Image>().color = Color.yellow;*/
        
        GameManager.inst.UpdateSelection(assignedLocation.GetMainLocation().gameObject);
    }
}
