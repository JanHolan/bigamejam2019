﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class RewardManager : MonoBehaviour
{
    [SerializeField] private GameObject choiceButton;

    [SerializeField] private GameObject buttonGridContainer;
    [SerializeField] private Button accept;
    private Queue<ChoiceButtonController> selected = new Queue<ChoiceButtonController>();

    [SerializeField] private GameObject anchorLeft;
    [SerializeField] private GameObject anchorRight;
    [SerializeField] private GameObject anchorMiddle;

    [SerializeField] private Image heroImage;

    [SerializeField] private Image locOld;

    [SerializeField] private Image locNew;

    [SerializeField] private TextMeshProUGUI hintText;
    [SerializeField] private TextMeshProUGUI speakText;
    // Start is called before the first frame update
    void Start()
    {
        accept.onClick.AddListener(AcceptChoice);
    }

    void AcceptChoice()
    {
        foreach (var bt in buttonGridContainer.GetComponentsInChildren<Button>())
        {
            bt.interactable = false;
            if(!selected.Contains(bt.GetComponent<ChoiceButtonController>()))
                FadeObject(bt.gameObject, 0.4f);
        }

        hintText.DOFade(0, 0.8f);
        StartCoroutine(RewardSelectionAnimation());

        accept.interactable = false;
        FadeObject(accept.gameObject, 0.4f);
        


    }

    IEnumerator RewardSelectionAnimation()
    {
        
            int choice = 0;
            if (Random.value <= 0.5f)
                choice++;
            var sl = selected.ToArray();
            locOld.sprite = sl[choice].myChoice.fromState.GetComponent<LocationStateManager>().currentState.image;
            locNew.sprite = sl[choice].myChoice.toState.image;

            yield return new WaitForSecondsRealtime(1f);
            MoveButtonsUp();


            yield return new WaitForSecondsRealtime(1f);
            float pause = 1f;
            speakText.text = "And so it will be!\nYour actions will never be forgotten, Hero!";
            for (int i = 0; i < 5 + choice; i++)
            {
                var scl = heroImage.transform.localScale;
                scl.x *= -1;
                heroImage.transform.localScale = scl;
                yield return new WaitForSecondsRealtime(pause);
                pause *= 0.5f;
            }

            locOld.DOFade(1f, 0.8f).SetUpdate(true);
            speakText.DOFade(0, 0.2f);
            yield return new WaitForSecondsRealtime(1f);

            


            FadeObject(sl[(choice + 1) % 2].gameObject, 0.8f);
            sl[choice].transform.DOMove(anchorMiddle.transform.position, 1).SetUpdate(true);
            yield return new WaitForSecondsRealtime(1);

            speakText.DOFade(1, 0.3f);
            locNew.DOFade(1, 0.8f).SetUpdate(true);
            locOld.DOFade(0, 1f).SetUpdate(true);
            yield return new WaitForSecondsRealtime(2);
            try
            {
            sl[choice].DoMyChoice();

            GameManager.inst.QGwloc.GetMainLocation().heroList.Remove(GameManager.inst.selectedHero);
            Destroy(GameManager.inst.selectedHero.gameObject);
        }
        catch (Exception ex)
        {
            
        }

        CleanUp();
    }

    void MoveButtonsUp()
    {
        var bts = selected.ToList();
        bts[0].transform.DOMove(anchorLeft.transform.position,0.5f).SetUpdate(true);
        bts[1].transform.DOMove(anchorRight.transform.position,0.5f).SetUpdate(true);
    }

    void CleanUp()
    {
        var scl = heroImage.transform.localScale;
        scl.x = Math.Abs(scl.x);
        heroImage.transform.localScale = scl;
        var cl = Color.white;
        cl.a = 0;
        locOld.color = cl;
        locNew.color = cl;
        selected.Clear();
        Time.timeScale = 1f;
        gameObject.SetActive(false);
        accept.interactable = true;
        accept.GetComponent<Image>().color = Color.white;
        var tm = accept.GetComponentInChildren<TextMeshProUGUI>();
        var col = tm.color;
        col.a = 1;
        tm.color = col;
        DOTween.defaultTimeScaleIndependent = false;
        
        TutorialManager.inst.tutorialEvent(TutorialTriggers.REWARD_SENT);
        hintText.color = Color.black;
        speakText.color = Color.black;
        speakText.text =
            "You have done well, Hero! As a reward, you get one wish from <b>The Ring of Bi-Decisioning</b> that can shape the world! What do you desire more?";
    }

    // Update is called once per frame
    void Update()
    {
        accept.interactable = selected.Count == 2;
    }

    public void StartReward()
    {
        DOTween.defaultTimeScaleIndependent = true;
        heroImage.sprite = GameManager.inst.selectedHero.heroImage;
        gameObject.SetActive(true);
        Time.timeScale = 0;

        FillChoiceButtons();
    }

    private void FillChoiceButtons()
    {
        foreach (Transform child in buttonGridContainer.transform)
            GameObject.Destroy(child.gameObject);
        
        foreach (var choice in GetSixPossibleStates())
        {
           var bt = GameObject.Instantiate(choiceButton, buttonGridContainer.transform);
           bt.GetComponentInChildren<ChoiceButtonController>().Initialize(choice,this);
           
        }
    }

    private StateLink[] GetSixPossibleStates()
    {
        List<StateLink> allStates = new List<StateLink>();

        foreach (var fsm in FindObjectsOfType<LocationStateManager>())
        {
            foreach (var state in fsm.states)
            {
                foreach (var link in state.links)
                {
                    if(link.toState != fsm.currentState)
                        allStates.Add(link);
                }
            }
        }

        return allStates.OrderBy(x => Random.value).Take(6).ToArray();
    }

    public void Select(ChoiceButtonController choice)
    {
        if (selected.Contains(choice))
        {
            choice.Deselect();
            if (selected.Peek() == choice)
                selected.Dequeue();
            else
            {
                selected.Enqueue(selected.Dequeue());
                selected.Dequeue();
            }
        }
        else
        {
            if (selected.Count == 2)
                selected.Dequeue().Deselect();
        
            selected.Enqueue(choice);
            choice.Select();
        }
        

    }

    private void FadeObject(GameObject btn, float time)
    {
        btn.GetComponent<Image>().DOFade(0, time).SetUpdate(true);
        btn.GetComponentInChildren<TextMeshProUGUI>().DOFade(0, time).SetUpdate(true);
    }
}
