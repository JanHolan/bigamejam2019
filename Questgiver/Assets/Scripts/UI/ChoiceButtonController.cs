﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChoiceButtonController : MonoBehaviour
{
    public StateLink myChoice;

    private RewardManager rw;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Initialize(StateLink choice, RewardManager man)
    {
        GetComponentInChildren<TextMeshProUGUI>().text = choice.linkName == "" ? ""+choice.fromState.stateName + " ->" + choice.toState.stateName : choice.linkName;
        myChoice = choice;
        rw = man;
    }

    public void OnClick()
    {
        rw.Select(this);
    }

    public void Deselect()
    {
        GetComponent<Image>().color = Color.white;
    }

    public void Select()
    {
        GetComponent<Image>().color = Color.yellow;
    }

    public void DoMyChoice()
    {
        myChoice.fromState.GetComponent<LocationStateManager>().ForceTransition(myChoice);
    }
}
