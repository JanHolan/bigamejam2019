﻿using UnityEngine;

public class QueueSelector : MonoBehaviour
{
    
    public void choose()
    {
        var location = GameManager.inst.QGwloc.GetMainLocation();
        int index;
        int.TryParse(name, out index);

        Debug.Log(index);
        Debug.Log(location.heroList.Count);
        if (index >= location.heroList.Count) return;
        var hero = location.heroList[index];
        GameManager.inst.changeHeroSelection(hero);
    }
}
