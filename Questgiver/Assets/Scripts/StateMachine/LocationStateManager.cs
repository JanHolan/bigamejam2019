﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LocationStateManager : MonoBehaviour
{
    [SerializeField]
    private StateBase startState;
    public StateBase currentState;
    public bool character;
    public StateBase[] states;

    public int maxHeroes = 5;
    public List<HeroController> heroList = new List<HeroController>();
    public GameObject heroPrefab;
    private GameObject exclamationQuest;
    private GameObject exclamationReward;
    private TextMeshPro timerText;
    private void Start()
    {
        exclamationQuest = transform.Find("ExclamationQuest")?.gameObject;
        exclamationReward = transform.Find("ExclamationReward")?.gameObject;
        
        if (exclamationQuest) 
            exclamationQuest.SetActive(false);
        if (exclamationReward)
            exclamationReward.SetActive(false);
        states = GetComponents<StateBase>();
        currentState = startState;

        foreach (Transform eachChild in transform)
        {
            if (eachChild.name == "Timer")
            {
                timerText = eachChild.GetComponent<TextMeshPro>();
            }
            
        }
        if (timerText != null)
            timerText.text = "";
    }

    private void Update()
    {
        if (!GameManager.inst.discoveredStates.Contains(currentState))
        {
            GameManager.inst.discoveredStates.Add(currentState);
        }
        
        if (heroList.Count <= 0)
        {
            if (exclamationQuest && exclamationReward)
            {
                exclamationQuest.SetActive(false);
                exclamationReward.SetActive(false);
                timerText.text = "";
            }
            return;
        }
        var wantQuest = false;
        var wantReward = false;
        var toRemove = new List<HeroController>();
        var failedReward = false;
        var failedWaiting = false;

        float worstTime = 500;
        foreach (var hero in heroList)
        {
           
            
                if (hero.timer.TimeLeft() < worstTime)
                {
                    worstTime = hero.timer.TimeLeft();
                    timerText.text = ((int) worstTime).ToString();
                }
            

            if (hero.timer.IsDone())
            {
                switch (hero.heroState)
                {
                    case HeroState.WaitingForQuest:
                        ChatManager.inst.GameChat(hero.heroName + " is tired of waiting for a quest!");

                        if (TutorialManager.inst.finished)
                        {
                            GameManager.inst.LifeLost("No quest received");
                        }
                        else
                        {
                            failedWaiting = true;
                        }
                        break;
                    case HeroState.WaitingForReward:
                        ChatManager.inst.GameChat(hero.heroName + " is tired of waiting for a reward!");
                        if (TutorialManager.inst.finished)
                        {
                            GameManager.inst.LifeLost("No reward given");
                        }
                        else
                        {
                            failedReward = true;
                        }
                        break;
                }

                toRemove.Add(hero);
                continue;
            }
            
            switch (hero.heroState)
            {
                case HeroState.WaitingForQuest:
                    wantQuest = true;
                    break;
                case HeroState.WaitingForReward:
                    wantReward = true;
                    break;
            }
        }

        foreach (var hero in toRemove)
        {
            heroList.Remove(hero);
            Destroy(hero.gameObject);
        }
        
        if (failedReward)
            TutorialManager.inst.FailedReward();
        else if (failedWaiting)
            TutorialManager.inst.FailedWait();

        exclamationQuest.SetActive(wantQuest && exclamationQuest);
        exclamationReward.SetActive(wantReward && exclamationReward);
    }

    public void AddReturningHero(HeroController hero)
    {
        if (heroList.Count < maxHeroes)
        {
            heroList.Add(hero);
        }
        else
        {
            var last = heroList[heroList.Count - 1];
            heroList.Remove(last);
            Destroy(last);
            heroList.Add(hero);
        }
        ChatManager.inst.GameChat(hero.heroName.Trim() + " wants a reward in " + name, false);
    }
    
    public bool AddHero()
    {
        if (heroList.Count >= maxHeroes) return false;

        var hero = Instantiate(heroPrefab, gameObject.transform.position, Quaternion.identity);
        var heroComponent = hero.GetComponent<HeroController>();
        heroList.Add(heroComponent);
        ChatManager.inst.GameChat(heroComponent.heroName.Trim() + " wants a quest in " + name, false);
        return true;
    }
    
    public bool TryTransition(StateBase toState, Action onComplete)
    {
        return currentState.TryTransition(toState, onComplete);
    }

    private StateBase stateTransition = null;

    public bool TryTransition(StateLink link, Action onComplete)
    {
        if (stateTransition != null)
            return false;

        StateBase nextState = currentState.TryTransition(link, () =>
        {
            OnStateTransitionDone();
            onComplete.Invoke();
        });
    if (nextState == null)
            return false;
        stateTransition = nextState;
        GetComponentInChildren<LocChangeController>().ChangeLoc(nextState.image);
        return true;
    }

    private void OnStateTransitionDone()
    {
        currentState = stateTransition;
        currentState.onStateEnter.Invoke();
        stateTransition = null;
    }
    

    public StateLink[] GetAllQuests()
    {
        List<StateLink> stateList = new List<StateLink>();
        foreach (var state in states)
        {
            if(state.enabled)
                foreach(var quest in state.GetQuests())
                stateList.Add(quest);
        }

        return stateList.ToArray();
    }





    public void ForceTransition(StateLink myChoice)
    {
        myChoice.onTransitionStart.Invoke();
        myChoice.fromState.onStateExit.Invoke();
        myChoice.toState.onStateEnter.Invoke();
        currentState = myChoice.toState;
    }
}
