﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[Serializable]
public class OnTransition : UnityEvent {}

[Serializable]
public class StateLink
{
    public StateBase toState;
    public float transitionDuration = 0;
    public string linkName;
    public string linkActionDescription;
    public string choiceSelectionButtonString;
    public string selectDescription;
    public OnTransition onTransitionStart;
    public bool enabled = true;
    public bool isQuest = true;
    [HideInInspector]
    public StateBase fromState;

    public bool IsActive(WorldLocationController forLocation)
    {
        return fromState.GetComponent<LocationStateManager>().currentState == fromState && fromState.transform.parent == forLocation.transform;
    }


}

public class StateBase : MonoBehaviour
{
    public string stateName;
    public string stateDescription;
    public Sprite image;
    [SerializeField]
    public StateLink[] links;
    public OnTransition onStateExit;
    public OnTransition onStateEnter;
    
    // Start is called before the first frame update
    void Start()
    {
        foreach (var link in links)
        {
            link.fromState = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool TryTransition(StateBase toState, Action onComplete)
    {
        foreach(var link in links)
            if (link.toState == toState)
            {
                StartCoroutine(Transition(link,() => {  onStateExit.Invoke(); onComplete(); }));
                return true;
            }

        return false;
    }


    protected virtual IEnumerator Transition(StateLink link, Action onComplete)
    {
        link.onTransitionStart.Invoke();
        yield return new WaitForSeconds(link.transitionDuration + 5f);
        onStateExit.Invoke();
        onComplete();
    }

    public StateLink[] GetQuests()
    {
        List<StateLink> quests = new List<StateLink>();
        foreach(var link in links)
            if(link.isQuest)
                quests.Add(link);

        return quests.ToArray();
    }

    public StateBase TryTransition(StateLink link, Action onComplete)
    {
        foreach(var myLink in links)
            if (link.linkName == myLink.linkName)
            {
                StartCoroutine(Transition(link,() => {  onStateExit.Invoke(); onComplete(); }));
                return link.toState;
            }

        return null;
    }
}
