﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    [SerializeField] private Vector2 offset = Vector2.zero;
    public void MoveToWorldAnchor(WorldLocationController worldAnchor)
    {
        transform.SetParent(worldAnchor.transform,true);
        transform.DOLocalJump(offset, 0.5f, 2, 1);
    }
    // Start is called before the first frame update
    void Start()
    {
        transform.localPosition = offset;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
