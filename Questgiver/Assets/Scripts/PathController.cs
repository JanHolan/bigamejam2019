﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class PathController
{

    [HideInInspector]
    public WorldLocationController from;

    public WorldLocationController to;

    public PathCreation.PathCreator path;
    
    
}
