﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    static public LevelManager instance;

    void Awake()
    {
        instance = this;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void closeTutorial()
    {
        TutorialManager.inst.CloseTutorial();
    }


    public void LoadScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
    
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("IntroScene");
    }

    public void EndGame()
    {
        Application.Quit();
    }
}
