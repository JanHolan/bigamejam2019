﻿using System.Collections;
using UnityEngine;

public enum TutorialTriggers
{
    HERO_APPEAR,
    HERO_QUEST_SELECT,
    HERO_QUEST_COMPLETED,
    HERO_BACK,
    REWARD_SCREEN,
    REWARD_SENT,
    FAIL
};


public class TutorialManager : MonoBehaviour
{
    public static TutorialManager inst;
    public int tutorialStep = 0;
    public bool failShown = false;
    public GameObject tutorial0;
    public GameObject tutorial1;
    public GameObject tutorial2;
    public GameObject tutorial3;
    public GameObject tutorial4;
    public GameObject tutorial5;
    public GameObject tutorial6;
    public GameObject tutorial7;
    public GameObject fail1;
    public GameObject fail2;
    public GameObject fail3;
    public bool finished;

    private void Start()
    {
        if (inst == null)
        {
            DontDestroyOnLoad(this);
            inst = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F5))
        {
            tutorialStep = 0;
            failShown = finished = false;
        }

        if (tutorial0 == null)
        {
            tutorial0 = GameObject.Find("Tutorial0");
            if (tutorial0 != null)
                tutorial0.SetActive(false);
        }

        if (tutorial1 == null)
        {
            tutorial1 = GameObject.Find("Tutorial1");
            if (tutorial1 != null)
                tutorial1.SetActive(false);
        }

        if (tutorial2 == null)
        {
            tutorial2 = GameObject.Find("Tutorial2");
            if (tutorial2 != null)
                tutorial2.SetActive(false);
        }

        if (tutorial3 == null)
        {
            tutorial3 = GameObject.Find("Tutorial3");
            if (tutorial3 != null)
                tutorial3.SetActive(false);
        }

        if (tutorial4 == null)
        {
            tutorial4 = GameObject.Find("Tutorial4");
            if (tutorial4 != null)
                tutorial4.SetActive(false);
        }

        if (tutorial5 == null)
        {
            tutorial5 = GameObject.Find("Tutorial5");
            if (tutorial5 != null)
                tutorial5.SetActive(false);
        }

        if (tutorial6 == null)
        {
            tutorial6 = GameObject.Find("Tutorial6");
            if (tutorial6 != null)
                tutorial6.SetActive(false);
        }

        if (tutorial7 == null)
        {
            tutorial7 = GameObject.Find("Tutorial7");
            if (tutorial7 != null)
                tutorial7.SetActive(false);
        }

        if (fail1 == null)
        {
            fail1 = GameObject.Find("Fail1");
            if (fail1 != null)
                fail1.SetActive(false);
        }

        if (fail2 == null)
        {
            fail2 = GameObject.Find("Fail2");
            if (fail2 != null)
                fail2.SetActive(false);
        }

        if (fail3 == null)
        {
            fail3 = GameObject.Find("Fail3");
            if (fail3 != null)
                fail3.SetActive(false);
        }

        if (tutorialStep == 0 && tutorial0)
        {
            StartCoroutine(IntroTutorial());
            tutorialStep++;
        }
    }

    public IEnumerator IntroTutorial()
    {
        yield return new WaitForSeconds(2f);
        tutorial0.SetActive(true);
        Time.timeScale = 0;
    }

    public void tutorialEvent(TutorialTriggers trigger)
    {
        if (!failShown && trigger == TutorialTriggers.FAIL)
        {
            tutorial7.SetActive(true);
            Time.timeScale = 0;
            failShown = true;
            return;
        }

        if (tutorialStep == 1 && trigger == TutorialTriggers.HERO_APPEAR)
        {
            tutorial1.SetActive(true);
            Time.timeScale = 0;
            tutorialStep++;
        }
        else if (tutorialStep == 2 && trigger == TutorialTriggers.HERO_QUEST_SELECT)
        {
            tutorial2.SetActive(true);
            Time.timeScale = 0;
            tutorialStep++;
        }
        else if (tutorialStep == 3 && trigger == TutorialTriggers.HERO_QUEST_COMPLETED)
        {
            tutorial3.SetActive(true);
            Time.timeScale = 0;
            tutorialStep++;
        }
        else if (tutorialStep == 4 && trigger == TutorialTriggers.HERO_BACK)
        {
            tutorial4.SetActive(true);
            Time.timeScale = 0;
            tutorialStep++;
        }
        else if (tutorialStep == 5 && trigger == TutorialTriggers.REWARD_SCREEN)
        {
            tutorial5.SetActive(true);
            Time.timeScale = 0;
            tutorialStep++;
        }
        else if (tutorialStep == 6 && trigger == TutorialTriggers.REWARD_SENT)
        {
            tutorial6.SetActive(true);
            Time.timeScale = 0;
            tutorialStep++;
            finished = true;
        }
    }

    public void FailedQuest()
    {
        fail1.SetActive(true);
        Time.timeScale = 0;
        GameManager.inst.SpawnHero();
    }

    public void FailedWait()
    {
        fail2.SetActive(true);
        Time.timeScale = 0;
        GameManager.inst.SpawnHero();
    }

    public void FailedReward()
    {
        fail3.SetActive(true);
        Time.timeScale = 0;
        GameManager.inst.SpawnHero();
    }

    public void CloseTutorial()
    {
        if (!tutorial5.activeSelf)
            Time.timeScale = 1;
        tutorial0.SetActive(false);
        tutorial1.SetActive(false);
        tutorial2.SetActive(false);
        tutorial3.SetActive(false);
        tutorial4.SetActive(false);
        tutorial5.SetActive(false);
        tutorial6.SetActive(false);
        tutorial7.SetActive(false);
        fail1.SetActive(false);
        fail2.SetActive(false);
        fail3.SetActive(false);
    }
}