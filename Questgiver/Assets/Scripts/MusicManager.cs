﻿using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public static MusicManager inst;
    private void Start()
    {
        if (inst == null)
        {
            DontDestroyOnLoad(this);
            inst = this;
        }
        else
        {
            Destroy(this);
        }
    }
}
