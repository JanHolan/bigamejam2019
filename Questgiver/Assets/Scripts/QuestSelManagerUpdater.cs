﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestSelManagerUpdater : MonoBehaviour
{
    // Start is called before the first frame update
    private SentToQuestButtonController stq;
    void Start()
    {
        stq = FindObjectOfType<SentToQuestButtonController>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!stq.gameObject.activeInHierarchy)
            stq.UpdateNow();
    }
}
