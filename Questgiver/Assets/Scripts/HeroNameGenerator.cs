﻿using System.Text.RegularExpressions;
using UnityEngine;

public class HeroNameGenerator : MonoBehaviour
{

    public static HeroNameGenerator inst;
    public TextAsset names;
    public TextAsset before;
    public TextAsset after;
    public TextAsset title;
    public TextAsset blabber;
    public TextAsset colors;

    public string[] beforeArr;
    public string[] namesArr;
    public string[] titleArr;
    public string[] afterArr;
    public string[] blabberArr;
    public string[] colorsArr;
    
    private void Awake()
    {
        inst = this;
        beforeArr = before.ToString().Split('\n');
        namesArr = names.ToString().Split('\n');
        titleArr = title.ToString().Split('\n');
        afterArr = after.ToString().Split('\n');
        blabberArr = blabber.ToString().Split('\n');
        colorsArr = colors.ToString().Split('\n');
    }
    
    public string GetBlabber()
    {
        var index = UnityEngine.Random.Range(0, blabberArr.Length);
        return blabberArr[index].Trim();
    }

    public string GetColor()
    {
        var index = UnityEngine.Random.Range(0, colorsArr.Length);
        return colorsArr[index].Trim();
    }

    public string GetName()
    {
        var charName = "";

        var type = UnityEngine.Random.Range(0, 2);
        if (type == 1)
        {
            var index = UnityEngine.Random.Range(0, beforeArr.Length);
            charName += beforeArr[index];
            index = UnityEngine.Random.Range(0, namesArr.Length);
            charName += namesArr[index];
            index = UnityEngine.Random.Range(0, afterArr.Length);
            charName += afterArr[index];
            
        }
        else
        {
            var index = UnityEngine.Random.Range(0, namesArr.Length);
            charName += namesArr[index];
            index = UnityEngine.Random.Range(0, titleArr.Length);
            charName += titleArr[index];
        }
        
        const string replacementString = "";
        charName = Regex.Replace(charName, @"\r\n?|\n", replacementString);
        
        return charName;
    }
}
