﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ChatManager : MonoBehaviour
{
    public static ChatManager inst;
    public int maxLines = 6;
    private readonly Queue<string> queue = new Queue<string>();
    private TextMeshProUGUI text;
    
    private void Start()
    {
        inst = this;
        text = GetComponentInChildren<TextMeshProUGUI>();
        ReloadScreen();
    }
    
    public void PlayerChat(string playerName, string message, string color = "black")
    {
        var str = "<color=" + color + ">[" + playerName + "] says: " + message + "</color>";
        if (queue.Count >= maxLines)
            queue.Dequeue();
        queue.Enqueue(str);
        ReloadScreen();
    }


    public void GameChat(string message, bool warning = true)
    {
        var str = warning ? "<b><color=\"red\">[" : "<b><color=\"green\">[";
        str += "GAME]: " + message + "</color></b>";
        if (queue.Count >= maxLines)
            queue.Dequeue();
        queue.Enqueue(str);
        ReloadScreen();
    }
    
    private void ReloadScreen()
    {
        var messages = "";
        foreach (var str in queue)
        {
            messages += str + "\n";
        }
        text.text = messages;
    }
}