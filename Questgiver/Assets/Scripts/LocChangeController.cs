﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class LocChangeController : MonoBehaviour
{

    [SerializeField] private SpriteRenderer[] bgNewOldImg;


    private bool fading;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var img in bgNewOldImg)
        {
            img.DOFade(0, 0f);
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FadeOut()
    {
        foreach (var img in bgNewOldImg)
        {
            img.DOFade(0, 0.4f);
        }
    }

    public void ChangeLoc(Sprite toImage)
    {
        foreach (var img in bgNewOldImg)
        {
            DOTween.Kill(img);
        }
        bgNewOldImg[2].sprite = GetComponentInParent<LocationStateManager>().currentState.image;
        StartCoroutine(ChangeRoutine(toImage));
    }

    IEnumerator ChangeRoutine(Sprite toImage)
    {
        bgNewOldImg[0].DOFade(1, 0.5f);
        bgNewOldImg[2].DOFade(1, 0.5f);
        yield return new WaitForSeconds(1.5f);
        bgNewOldImg[1].sprite = toImage;
        bgNewOldImg[1].DOFade(1, 2);
        bgNewOldImg[2].DOFade(0, 2.2f);
        yield return new WaitForSeconds(2.7f);
        FadeOut();
        yield return new WaitForSeconds(0.2f);
        bgNewOldImg[2].sprite = toImage;
    }
}
