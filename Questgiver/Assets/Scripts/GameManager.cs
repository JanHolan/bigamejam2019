﻿﻿using System.Collections;
using System.Collections.Generic;
﻿using System;
using DG.Tweening;
using Silicomrades;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public static GameManager inst;
    public HashSet<StateBase> discoveredStates = new HashSet<StateBase>();
    public bool playing = true;
    private int lives = 3;
    private int score = 0;
    public float teleportTime = 2f;
    private bool teleporting = false;
    private TextMeshProUGUI scoreText;
    
    public GameObject QG;
    public WorldLocationController QGwloc;
    public GameObject selection;
    public HeroController selectedHero;

    public LocationStateManager[] locations;

    private TextMeshProUGUI selectionName;
    private TextMeshProUGUI selectionStatus;
    private Image selectionImage;

    public TextMeshProUGUI[] queueTimers;
    public TextMeshProUGUI[] queueNames;
    public Image[] queueImages;
    public GameObject[] queueActive;
    
    private Timer toNextSpawn;
    public float spawnMin = 5;
    public float spawnMax = 15;

    public GameObject[] crosses;
    public GameObject endScreen;
    public TextMeshProUGUI endScore;
    public TextMeshProUGUI endLocations;
    public bool firstSpawned;

    [SerializeField] private RewardManager rm;

    private void Start()
    {
        inst = this;
        Time.timeScale = 1;
        endScreen.SetActive(false);
        QG = GameObject.Find("QG");
        QG.transform.position = QGwloc.transform.position;
      
        toNextSpawn = new Timer(8);
        
        scoreText = GameObject.Find("Score").GetComponent<TextMeshProUGUI>();
        scoreText.text = score.ToString();
        selectionName = GameObject.Find("SelectionName").GetComponent<TextMeshProUGUI>();
        selectionStatus = GameObject.Find("SelectionStatus").GetComponent<TextMeshProUGUI>();
        selectionImage = GameObject.Find("SelectionImage").GetComponent<Image>();
        UpdateSelection();
        UpdateQueue();

        foreach (var cross in crosses)
        {
            cross.SetActive(false);
        }
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F3))
        {
            bool spawned;
            int tries = 0;
            do
            {
                tries++;
                if (tries > 100)
                {
                    break;
                }
                var index = Random.Range(0, locations.Length);
                spawned = locations[index].AddHero();
            } while (spawned == false);
        }

        if (Input.GetMouseButtonDown(0))
        {
            LocationStateManager[] locations = FindObjectsOfType<LocationStateManager>();
            GameObject tMin = null;
            float minDist = Mathf.Infinity;
            Vector3 currentPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            foreach (LocationStateManager t in locations)
            {
                float dist = Vector3.Distance(t.transform.position, currentPos);
                if (dist < minDist)
                {
                    tMin = t.gameObject;
                    minDist = dist;
                }
            }
            
            HeroController[] heroes = FindObjectsOfType<HeroController>();
            foreach (HeroController t in heroes)
            {
                float dist = Vector3.Distance(t.transform.position, currentPos);
                if (dist < minDist)
                {
                    tMin = t.gameObject;
                    minDist = dist;
                }
            }
            
            
            
            if (tMin != null && minDist < 10.016)
            {
                UpdateSelection(tMin);
            }
        }
        UpdateSelection(selection);
        
        if (selectedHero != null && selectedHero.heroState == HeroState.Questing)
            selectedHero = null;
        
        UpdateQueue();
        toNextSpawn.DoUpdate();
        
        if (!toNextSpawn.IsDone()) return;

        if (!firstSpawned || TutorialManager.inst.finished)
        {
            SpawnHero();

            float newSpawnMin = spawnMin - (float) Math.Ceiling((decimal) (score / 8));
            float newSpawnMax = spawnMax - (float) Math.Ceiling((decimal) (score / 4));

            if (newSpawnMin <= 1) newSpawnMin = 1;
            if (newSpawnMax <= 2) newSpawnMax = 2;

            toNextSpawn.Reset(Random.Range(newSpawnMin, newSpawnMax));
        }
    }

    public void SpawnHero()
    {
        firstSpawned = true;
        bool spawned;
        int tries = 0;
        do
        {
            tries++;
            if (tries > 100)
            {
                break;
            }
            var index = Random.Range(0, locations.Length);
            spawned = locations[index].AddHero();
        } while (spawned == false);
        TutorialManager.inst.tutorialEvent(TutorialTriggers.HERO_APPEAR);
    }

    private void UpdateQueue()
    {
        var location = QGwloc.GetMainLocation();

        if (!location.heroList.Contains(selectedHero))
            selectedHero = null;
        
       /* if (selectedHero == null && location.heroList.Count > 0)
            selectedHero = location.heroList[0];*/
        
        for (var i = 0; i < location.maxHeroes; i++)
        {
            queueActive[i].SetActive(false);
            if (i < location.heroList.Count)
            {
                var hero = location.heroList[i];
                queueImages[i].enabled = queueNames[i].enabled = queueTimers[i].enabled = true;
                queueImages[i].sprite = hero.heroImage;
                queueTimers[i].text = Math.Round(hero.timer.TimeLeft()).ToString();
                queueNames[i].text = hero.heroName;

                if (hero == selectedHero)
                {
                    queueActive[i].SetActive(true);
                    if (hero.heroState == HeroState.WaitingForReward)
                    {
                        queueActive[i].GetComponent<Image>().color = Color.blue;
                    }
                    else
                    {
                        queueActive[i].GetComponent<Image>().color = Color.yellow;
                    }                    
                }
            }
            else
            {
                queueImages[i].enabled = queueNames[i].enabled = queueTimers[i].enabled = false;
            }
        }
    }

    public void UpdateSelection(GameObject newSelection = null)
    {
        selection = newSelection;

        if (selection == null)
        {
            selectionName.text = "None";
            selectionStatus.text = "Use LEFT CLICK to select location or character on map";
            selectionImage.sprite = null;
            selectionImage.enabled = false;
        }
        else
        {
            selectionImage.enabled = true;
            var location = selection.GetComponent<LocationStateManager>();
            var hero = selection.GetComponent<HeroController>();
            if (location != null)
            {
                selectionName.text = location.name;
                selectionStatus.text = location.currentState.stateDescription == "" ? "MISSING LOCATION STATUS TEXT!" : location.currentState.stateDescription;
                selectionImage.sprite = location.currentState.image;
            } else if (hero != null)
            {
                selectionName.text = hero.heroName;
                selectionImage.sprite = hero.heroThumbImage;
                if (hero.heroState == HeroState.Questing)
                {
                    selectionStatus.text = "Heading to " + hero.questLocation.GetMainLocation().name + " to " + (hero.currentQuest.selectDescription == "" ? "Missing text" : hero.currentQuest.selectDescription + ".");
                }
                else
                {
                    selectionStatus.text = "Coming back to " + hero.questStartLocation.GetMainLocation().name;
                }
            }
        }
    }
    
    public void QuestCompleted(string playerName, int val = 1)
    {
        score += val;
        ChatManager.inst.GameChat("Player " + playerName + " completed quest! +1 point!", false);
        scoreText.text = score.ToString();
        TutorialManager.inst.tutorialEvent(TutorialTriggers.HERO_QUEST_COMPLETED);
    }

    public void changeHeroSelection(HeroController hero)
    {
        TutorialManager.inst.tutorialEvent(TutorialTriggers.HERO_QUEST_SELECT);
        selectedHero = hero;
        UpdateQueue();
    }
    
    public void MoveCGToLocation(WorldLocationController loc)
    {
        if (loc == QGwloc || teleporting)
            return;
        
        //TODO QG animace teleportu
        teleporting = true;
        var coroutine = MoveCGToLocationCoroutine(loc);
        StartCoroutine(coroutine);
    }

    public IEnumerator MoveCGToLocationCoroutine(WorldLocationController loc)
    {
        QG.GetComponentInChildren<ParticleSystem>().Play();
        loc.GetComponentInChildren<ParticleSystem>().Play();
        yield return new WaitForSeconds(teleportTime);
        FindObjectOfType<QuestSelectionManager>().Deselect();
        QG.transform.position = loc.transform.position;
        QGwloc = loc;
        teleporting = false;
        //TODO QG animace teleportu
    }
    
    public void LifeLost(string bug)
    {
        lives--;
        TutorialManager.inst.tutorialEvent(TutorialTriggers.FAIL);
        if (lives >= 0)
        {
            ChatManager.inst.GameChat("Life lost!");
            crosses[lives].transform.Find("BugText").GetComponent<TextMeshProUGUI>().text = bug;
            crosses[lives].SetActive(true);
        }
        
        if (lives <= 0)
            GameOver();
    }

    private void GameOver()
    {
        endScore.text = endScore.text + score + " players</u>.";
        endLocations.text = endLocations.text + discoveredStates.Count + " out of 28</u> different world states.";
        endScreen.SetActive(true);
        Time.timeScale = 0f;
        DOTween.timeScale = 1f;
        playing = false;
    }
    
    public StateLink[] GetAllCharacterQuests()
    {
        List<StateLink> stateList = new List<StateLink>();
        foreach (var loc in FindObjectsOfType<LocationStateManager>())
        {
            if (!loc.character)
                continue;
            foreach(var quest in loc.GetAllQuests())
                stateList.Add(quest);
        }

        return stateList.ToArray();
    }

    public void GiveRewardToHero()
    {
        TutorialManager.inst.tutorialEvent(TutorialTriggers.REWARD_SCREEN);
        rm.StartReward();
    }
    
}