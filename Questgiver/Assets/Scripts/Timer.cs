﻿using UnityEngine;

namespace Silicomrades
{
    public class Timer
    {
        public float duration;
        float currentTime;

        public Timer(float durationInSeconds, bool ready = true)
        {
            duration = durationInSeconds;
            if (!ready) currentTime = duration;
        }

        public void Reset()
        {
            currentTime = 0;
        }

        public void Reset(float newDuration)
        {
            duration = newDuration;
            currentTime = 0;
        }

        public void DoUpdate()
        {
            if (!IsDone())
                currentTime += Time.deltaTime;
        }

        public void DoFixedUpdate()
        {
            if (!IsDone())
                currentTime += Time.fixedDeltaTime;
        }

        public bool IsDone()
        {
            return currentTime >= duration;
        }

        public float TimeLeft()
        {
            return duration - currentTime;
        }

        public void Finish()
        {
            currentTime = duration;
        }

        public void Extend(float f)
        {
            duration += f;
        }
    }

}